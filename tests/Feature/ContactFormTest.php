<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactFormTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     * @test
     */
    public function it_validates_the_submission_stores_the_submission_and_returns_success()
    {
        $submission = factory('App\ContactFormSubmission')->make()->toArray();

        $response = $this->post('/contact-form-submit', ['data' => $submission]);

        $this->assertDatabaseHas('contact_form_submissions', [
            'name' => $submission['name'],
            'email' => $submission['email'],
            'phone' => $submission['phone'],
            'comments' => $submission['comments']
        ]);

        $response->assertStatus(200);
    }
}
