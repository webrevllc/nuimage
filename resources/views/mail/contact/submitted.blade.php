@component('mail::message')
# Contact Us Form Submitted

## We have received your information.

@component('mail::panel')
**Name:** {{ $name }} <br/>
**Email:** {{ $email }} <br/>
**Phone:** {{ $phone }} <br/>
**Comments/Questions:** {{ $comments }}
@endcomponent


Thanks,<br>
Nu Image Contact Us Form
@endcomponent
