<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Nu-Image</title>

        <link href="{{mix('/css/app.css')}}" rel="stylesheet" />

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    </head>
    <body class="h-screen">
        <div class="stars large"></div>
        <div class="container max-w-lg mx-auto  h-full flex justify-center items-center" id="app">
            <contact-form></contact-form>
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>

    </body>
</html>
