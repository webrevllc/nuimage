<?php

namespace App\Http\Controllers;

use App\ContactFormSubmission;
use App\Notifications\SubmissionCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class ContactFormController extends Controller
{
    public function submit(Request $request)
    {
        $validator = Validator::make($request->data, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'comments' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 422);
        }

        $submission = ContactFormSubmission::create($request->data);

        $submission->sendEmailNotifications($request->data);

        return response('success');

    }

    public function index()
    {
        return ContactFormSubmission::get();
    }
}
