<?php

namespace App;

use App\Notifications\SubmissionCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class ContactFormSubmission extends Model
{
    protected $guarded = [];

    public function sendEmailNotifications($data)
    {
        Notification::route('mail', $data['email'])->notify(new SubmissionCreated($data));
        Notification::route('mail', 'webrevllc@gmail.com')->notify(new SubmissionCreated($data));
    }
}
