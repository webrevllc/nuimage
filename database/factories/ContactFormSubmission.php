<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ContactFormSubmission;
use Faker\Generator as Faker;

$factory->define(ContactFormSubmission::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->numberBetween(1000000000, 9999999999),
        'comments' => $faker->paragraph
    ];
});
